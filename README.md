# README #

* Bit Dungeon
* Version: 0.0.0.3
* http://www.roguebasin.com/index.php?title=Complete_Roguelike_Tutorial,_using_python%2Blibtcod

### Set Up ###

* Run starty.py

Needed:
w9xpopen.exe, libcod-mingw.dll, SDL.dll, arial10x10.png, and background.png

## Creating .exe ##

* Run new.py
* .exe is saved in dist
* Create a .bat that runs said .exe
* Compile the .bat including the starty.exe, w9xpopen.exe, libcod-mingw.dll, SDL.dll, arial10x10.png, and background.png
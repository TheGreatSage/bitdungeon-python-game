import libtcodpy as libtcod
import math
import textwrap
import shelve
#############################################
# Variables
#############################################
SCREEN_WIDTH = 125  # Window Size
SCREEN_HEIGHT = 75  # Window Height
LIMIT_FPS = 30  # Max FPS
MAP_WIDTH = 300  # Map WIDTH
MAP_HEIGHT = 100  # Map Height
ROOM_MAX_SIZE = 15
ROOM_MIN_SIZE = 10
MAX_ROOMS = 90
FOV_ALGO = 0
FOV_LIGHT_WALLS = True
TORCH_RADIUS = 10
PLAYER_SPEED = 5
DEFAULT_SPEED = 8
DEFAULT_ATTACK_SPEED = 20
BAR_WIDTH = 20
PANEL_HEIGHT = 25
PANEL_Y = SCREEN_HEIGHT - PANEL_HEIGHT
MSG_X = BAR_WIDTH + 2
MSG_WIDTH = SCREEN_WIDTH - BAR_WIDTH - 2
MSG_HEIGHT = PANEL_HEIGHT - 1
INVENTORY_SIZE = 26
INVENTORY_WIDTH = 50
POWER_BOTTLE = 2
HEAL_AMOUNT = 40
LIGHTNING_DAMAGE = 40
LIGHTNING_RANGE = 5
FIREBALL_RADIUS = 3
FIREBALL_DAMAGE = 25
CONFUSE_NUM_TURNS = 30
CONFUSE_RANGE = 8
GAME_MESSAGE = 'Welcome to bit dungeon!\n I > inventory \n D > Drop \n SPACE > Pick up'
LEVEL_UP_BASE = 200
LEVEL_UP_FACTOR = 150
LEVEL_SCREEN_WIDTH = 40
CHARACTER_SCREEN_WIDTH = 30
CAMERA_WIDTH = 150
CAMERA_HEIGHT = 30
color_dark_wall = libtcod.Color(0, 0, 100)
color_light_wall = libtcod.Color(130, 110, 50)
color_dark_ground = libtcod.Color(50, 50, 150)
color_light_ground = libtcod.Color(200, 180, 50)
color_health_bar_green = libtcod.Color(0, 255, 0)


#############################################
# Objects
#############################################


#############################################
# Map Classes
#############################################


class Tile:

    def __init__(self, blocked, block_sight=None, name=None):
        self.blocked = blocked
        self.explored = False
        self.name = name
        if block_sight is None:
            block_sight = blocked
        self.block_sight = block_sight


class Rect:

    def __init__(self, x, y, w, h):
        self.x1 = x
        self.y1 = y
        self.x2 = x + w
        self.y2 = y + h

    def center(self):
        center_x = (self.x1 + self.x2) / 2
        center_y = (self.y1 + self.y2) / 2
        return (center_x, center_y)

    def intersect(self, other):
        return (self.x1 <= other.x2 and self.x2 >= other.x1 and self.y1 <= other.y2 and self.y2 >= other.y1)


class Object:  # Player, NPC, Items, Ect

    def __init__(self, x, y, char, name, color, blocks=False, fighter=None, ai=None, speed=DEFAULT_SPEED, item=None, always_visible=False, equipment=None):
        self.x = x
        self.y = y
        self.char = char
        self.color = color
        self.name = name
        self.blocks = blocks
        self.fighter = fighter
        self.ai = ai
        self.speed = speed
        self.wait = 0
        self.item = item
        self.always_visible = always_visible
        self.equipment = equipment
        if self.equipment:
            self.equipment.owner = self
            self.item = Item()
            self.item.owner = self
        if self.fighter:
            self.fighter.owner = self
        if self.ai:
            self.ai.owner = self
        if self.item:
            self.item.owner = self

    def move(self, dx, dy):  # Self, DirectionX, DirectionY
        if not is_blocked(self.x + dx, self.y + dy):
            self.x += dx
            self.y += dy
        self.wait = self.speed

    def move_ai(self, dx, dy):  # Self, DirectionX, DirectionY
        # Move RIGHT+UP
        if not is_blocked(self.x + dx, self.y + dy):
            self.x += dx
            self.y += dy
        else:
            dx = libtcod.random_get_int(0, -1, 1)
            dy = libtcod.random_get_int(0, -1, 1)
            if not is_blocked(self.x + dx, self.y + dy):
                self.x += dx
                self.y += dy

        self.wait = self.speed

    def draw(self):
        if ((libtcod.map_is_in_fov(fov_map, self.x, self.y)) or (self.always_visible and map[self.x][self.y].explored)):
            (x, y) = to_camera_coordinates(self.x, self.y)
            if x is not None:
                libtcod.console_set_default_foreground(con, self.color)
                libtcod.console_put_char(con, x, y, self.char, libtcod.BKGND_NONE)

    def clear(self):
        (x, y) = to_camera_coordinates(self.x, self.y)
        if x is not None:
            libtcod.console_put_char(con, x, y, ' ', libtcod.BKGND_NONE)

    def move_towards(self, target_x, target_y):
        (dx, dy) = (target_x - self.x, target_y - self.y)
        distance = math.sqrt(dx ** 2 + dy ** 2)

        dx = int(round(dx / distance))
        dy = int(round(dy / distance))
        self.move_ai(dx, dy)

    def distance_to(self, other):
        dx = other.x - self.x
        dy = other.y - self.y
        return math.sqrt(dx ** 2 + dy ** 2)

    def send_to_back(self):
        global objects
        objects.remove(self)
        objects.insert(0, self)

    def distance(self, x, y):
        return math.sqrt((x - self.x) ** 2 + (y - self.y) ** 2)


class Fighter:

    def __init__(self, hp, xp, defense, power, death_function=None, attack_speed=DEFAULT_ATTACK_SPEED):
        self.base_max_hp = hp
        self.hp = hp
        self.base_defense = defense
        self.death_function = death_function
        self.attack_speed = attack_speed
        self.xp = xp
        self.base_power = power

    @property
    def power(self):
        bonus = sum(equipment.power_bonus for equipment in get_all_equipped(self.owner))
        return self.base_power + bonus

    @property
    def defense(self):
        bonus = sum(equipment.defense_bonus for equipment in get_all_equipped(self.owner))
        return self.base_defense + bonus

    @property
    def max_hp(self):
        bonus = sum(equipment.max_hp_bonus for equipment in get_all_equipped(self.owner))
        return self.base_max_hp + bonus

    def take_damage(self, damage):
        if damage > 0:
            self.hp -= damage
            if self.hp <= 0:
                function = self.death_function
                if function is not None:
                    function(self.owner)
                if self.owner != player:
                    player.fighter.xp += self.xp

    def attack(self, target):
        damage = libtcod.random_get_int(0, self.power - 1, self.power + 1) - libtcod.random_get_int(0, target.fighter.defense - 1, target.fighter.defense + 1)
        if damage > 0:
            target.fighter.take_damage(damage)
            if target.name == 'player':
                message(str(self.owner.name.capitalize() + ' attacks ' + target.name + ' for ' + str(damage) + ' hit points.'), libtcod.red)
            else:
                message(str(self.owner.name.capitalize() + ' attacks ' + target.name + ' for ' + str(damage) + ' hit points.'), libtcod.dark_orange)
        else:
            None
        self.owner.wait = self.attack_speed

    def heal(self, amount):
        self.hp += amount
        if self.hp > self.max_hp:
            self.hp = self.max_hp

    def rage(self, pow=0, denf=0):
        if not pow == 0:
            self.power += pow
        if not denf == 0:
            self.defense += denf


class BasicMonster:

    def take_turn(self):
        monster = self.owner
        if libtcod.map_is_in_fov(fov_map, monster.x, monster.y):
            if monster.distance_to(player) >= 2:
                monster.move_towards(player.x, player.y)
            elif player.fighter.hp > 0:
                monster.fighter.attack(player)


class ConfusedMonster:

    def __init__(self, old_ai, num_turns=CONFUSE_NUM_TURNS):
        self.old_ai = old_ai
        self.num_turns = num_turns

    def take_turn(self):
        if self.num_turns > 0:
            self.owner.move(libtcod.random_get_int(0, -1, 1), libtcod.random_get_int(0, -1, 1))
            self.num_turns -= 1
        else:
            self.owner.ai = self.old_ai
            message(self.owner.name + 'is not confuzzeled anymore', libtcod.red)


class Equipment:

    def __init__(self, slot, power_bonus=0, defense_bonus=0, max_hp_bonus=0):
        self.power_bonus = power_bonus
        self.defense_bonus = defense_bonus
        self.max_hp_bonus = max_hp_bonus
        self.slot = slot
        self.is_equipped = False

    def toggle_equip(self):
        if self.is_equipped:
            self.dequip()
        else:
            self.equip()

    def equip(self):
        old_equipment = get_equipped_in_slot(self.slot)
        if old_equipment is not None:
            old_equipment.dequip()
        self.is_equipped = True
        message('Equipped ' + self.owner.name + ' on ' + self.slot + '!', libtcod.light_green)

    def dequip(self):
        if not self.is_equipped:
            return
        self.is_equipped = False
        message('Dequipped ' + self.owner.name + ' from ' + self.slot + '.', libtcod.light_yellow)


def get_all_equipped(obj):
    if obj == player:
        equipped_list = []
        for item in inventory:
            if item.equipment and item.equipment.is_equipped:
                equipped_list.append(item.equipment)
        return equipped_list
    else:
        return []


def get_equipped_in_slot(slot):
    for obj in inventory:
        for obj in inventory:
            if obj.equipment and obj.equipment.slot == slot and obj.equipment.is_equipped:
                return obj.equipment
        return None


class Item:

    def __init__(self, use_function=None):
        self.use_function = use_function

    def pick_up(self):
        if len(inventory) >= INVENTORY_SIZE:
            message('Inventory is full, drop your crap!', libtcod.red)
        else:
            inventory.append(self.owner)
            objects.remove(self.owner)
            message('Picked up ' + self.owner.name + '!', libtcod.red)
            equipment = self.owner.equipment
            if equipment and get_equipped_in_slot(equipment.slot) is None:
                equipment.equip()

    def use(self):
        if self.owner.equipment:
            self.owner.equipment.toggle_equip()
            return
        if self.use_function is None:
            message('That item is trash!')
        else:
            if self.use_function() != 'cancelled':
                inventory.remove(self.owner)

    def drop(self):
        objects.append(self.owner)
        inventory.remove(self.owner)
        self.owner.x = player.x
        self.owner.y = player.y
        message('Dropped ' + self.owner.name + '.',  libtcod.yellow)
        if owner.equipment:
            self.owner.equipment.dequip()


def is_blocked(x, y):
    if map[x][y].blocked:
        return True
    for object in objects:
        if object.blocks and object.x == x and object.y == y:
            return True
    return False


def create_room(room):
    global map
    for x in range(room.x1 + 1, room.x2):
        for y in range(room.y1 + 1, room.y2):
            map[x][y].blocked = False
            map[x][y].block_sight = False
            map[x][y].name = 'Room'


def create_h_tunnel(x1, x2, y1, y2):
    global map
    for x in range(min(x1, x2), max(x1, x2) + 1):
        for y in range(min(y1, y2), max(y1, y2) + 1):
            map[x][y].blocked = False
            map[x][y].block_sight = False
            map[x][y].name = 'Hallway'


def create_v_tunnel(y1, y2, x1, x2):
    global map
    for y in range(min(y1, y2), max(y1, y2) + 1):
        for x in range(min(x1, x2), max(x1, x2) + 1):
            map[x][y].blocked = False
            map[x][y].block_sight = False
            map[x][y].name = 'Hallway'

#############################################
# Map
#############################################


def make_map():
    global map, objects, stairs
    map = [[Tile(True) for y in range(MAP_HEIGHT)] for x in range(MAP_WIDTH)]  # Creating a multidimensional array in one line
    objects = [player]
    rooms = []
    num_rooms = 0
    for r in range(MAX_ROOMS):
        w = libtcod.random_get_int(0, ROOM_MIN_SIZE, ROOM_MAX_SIZE)
        h = libtcod.random_get_int(0, ROOM_MIN_SIZE, ROOM_MAX_SIZE)
        x = libtcod.random_get_int(0, 0, MAP_WIDTH - w - 1)
        y = libtcod.random_get_int(0, 0, MAP_HEIGHT - h - 1)

        new_room = Rect(x, y, w, h)

        failed = False
        for other_room in rooms:
            if new_room.intersect(other_room):
                failed = True
                break
        if not failed:
            create_room(new_room)    # Room Creator
            if not num_rooms == 0:
                place_objects(new_room)  # Object Spawner
            (new_x, new_y) = new_room.center()
            if num_rooms == 0:
                player.x = new_x
                player.y = new_y
            else:
                (prev_x, prev_y) = rooms[num_rooms - 1].center()
                if libtcod.random_get_int(0, 0, 1) == 1:
                    create_h_tunnel(prev_x, new_x, prev_y, prev_y + 1)
                    create_v_tunnel(prev_y, new_y, new_x, new_x + 1)
                else:
                    create_v_tunnel(prev_y, new_y, prev_x, prev_x + 1)
                    create_h_tunnel(prev_x, new_x, new_y, new_y + 1)
            rooms.append(new_room)
            num_rooms += 1
    stairs = Object(new_x, new_y, '<', 'stairs', libtcod.white)
    stairs.always_visible = True
    objects.append(stairs)
    stairs.send_to_back()


def place_objects(room):

    max_monsters = from_dungeon_level([[2, 1], [3, 4], [5, 6]])

    monster_chances = {}
    monster_chances['orc'] = 90
    monster_chances['troll'] = from_dungeon_level([[15, 3], [30, 5], [60, 7]])
    max_items = from_dungeon_level([[1, 1], [2, 4]])
    item_chances = {}
    item_chances['sword'] = from_dungeon_level([[5, 4]])
    item_chances['lightning'] = from_dungeon_level([[25, 4]])
    item_chances['confuse'] = from_dungeon_level([[10, 2]])
    item_chances['rage'] = from_dungeon_level([[25, 4]])
    item_chances['srage'] = from_dungeon_level([[15, 8]])
    item_chances['fireball'] = from_dungeon_level([[25, 6]])
    item_chances['sheild'] = from_dungeon_level([[15, 8]])
    item_chances['heal'] = 40

    # Items
    num_items = libtcod.random_get_int(0, 0, max_items)
    for i in range(num_items):
        x = libtcod.random_get_int(0, room.x1 + 1, room.x2 - 1)
        y = libtcod.random_get_int(0, room.y1 + 1, room.y2 - 1)
        if not is_blocked(x, y):
            choice = random_choice(item_chances)
            if choice == 'rage':  # 40%
                item_compnent = Item(use_function=cast_rage)
                item = Object(x, y, '!', 'rage Potion', libtcod.violet, item=item_compnent)
            elif choice == 'srage':  # 30%
                item_compnent = Item(use_function=cast_srage)
                item = Object(x, y, '+', 'super RAGE Potion', libtcod.dark_violet, item=item_compnent)
            elif choice == 'lightning':
                item_compnent = Item(use_function=cast_lightning)
                item = Object(x, y, '#', 'scroll of lightning', libtcod.light_yellow, item=item_compnent)
            elif choice == 'fireball':  # 20%
                item_compnent = Item(use_function=cast_fireball)
                item = Object(x, y, '#', 'scroll of fireball', libtcod.light_yellow, item=item_compnent)
            elif choice == 'confuse':  # 20%
                item_compnent = Item(use_function=cast_confuse)
                item = Object(x, y, '#', 'scroll of confusion', libtcod.light_yellow, item=item_compnent)
            elif choice == 'sword':  # 20%
                equipment_compnent = Equipment(slot='Right Hand', power_bonus=3)
                item = Object(x, y, '/', 'sword', libtcod.sky, equipment=equipment_compnent)
            elif choice == 'sheild':  # 20%
                equipment_compnent = Equipment(slot='Left Hand', defense_bonus=2)
                item = Object(x, y, '[', 'sheild', libtcod.sky, equipment=equipment_compnent)
            elif choice == 'heal':  # 20%
                item_compnent = Item(use_function=cast_heal)
                item = Object(x, y, '!', 'health pot', libtcod.dark_red, item=item_compnent)
            objects.append(item)
            item.always_visible = True
            item.send_to_back()

    # Mobs
    num_monsters = libtcod.random_get_int(0, 0, max_monsters)
    for i in range(num_monsters):
        x = libtcod.random_get_int(0, room.x1 + 1, room.x2 - 1)
        y = libtcod.random_get_int(0, room.y1 + 1, room.y2 - 1)
        if not is_blocked(x, y):
            choice = random_choice(monster_chances)  # Out of 100 Makes it Percentage
            if choice == 'orc':  # 60%
                f_component = Fighter(20, 100, 0, 4, death_function=monster_death)
                ai_component = BasicMonster()
                monster = Object(x, y, 'o', 'orc', libtcod.desaturated_green, blocks=True, fighter=f_component, ai=ai_component)
            elif choice == 'troll':  # 30%
                f_component = Fighter(30, 350, 2, 8, death_function=monster_death)
                ai_component = BasicMonster()
                monster = Object(x, y, 'T', 'troll', libtcod.darker_green, blocks=True, fighter=f_component, ai=ai_component)
            objects.append(monster)


#############################################
# Rendering
#############################################


def render_bar(x, y, total_width, name, value, maximum, bar_color, back_color):
    bar_width = int(float(value) / maximum * total_width)
    libtcod.console_set_default_background(panel, back_color)
    libtcod.console_rect(panel, x, y, total_width, 1, False, libtcod.BKGND_SCREEN)
    libtcod.console_set_default_background(panel, bar_color)
    if bar_width > 0:
        libtcod.console_rect(panel, x, y, bar_width, 1, False, libtcod.BKGND_SCREEN)
    libtcod.console_set_default_foreground(panel, libtcod.black)
    libtcod.console_print_ex(panel, x + total_width / 2, y, libtcod.BKGND_NONE, libtcod.CENTER, name + ': ' + str(value) + '/' + str(maximum))


def get_names_under_mouse():
    global mouse
    (x, y) = (mouse.cx, mouse.cy)
    (x, y) = (camera_x + x, camera_y + y,)
    names = [obj.name for obj in objects if obj.x == x and obj.y == y and libtcod.map_is_in_fov(fov_map, obj.x, obj.y)]
    names = ', '.join(names)
    return names.capitalize()


def move_camera(target_x, target_y):
    global camera_x, camera_y, fov_recompute
    x = target_x - CAMERA_WIDTH / 2
    y = target_y - CAMERA_HEIGHT / 2

    if x < 0:
        x = 0
    if y < 0:
        y = 0
    if x > MAP_WIDTH - CAMERA_WIDTH - 1:
        x = MAP_WIDTH - CAMERA_WIDTH - 1
    if y > MAP_HEIGHT - CAMERA_HEIGHT - 1:
        y = MAP_HEIGHT - CAMERA_HEIGHT - 1

    if x != camera_x or y != camera_y:
        fov_recompute = True

    (camera_x, camera_y) = (x, y)


def to_camera_coordinates(x, y):
    (x, y) = (x - camera_x, y - camera_y)
    if (x < 0 or y < 0 or y >= CAMERA_WIDTH or y >= CAMERA_HEIGHT):
        return (None, None)
    return (x, y)


def get_player_pos():
    global player
    (x, y) = (player.x, player. y)
    names = [map[x][y].name]
    names = ', '.join(names)
    return names


def render_all():
    global fov_map, color_dark_wall, color_light_wall
    global fov_recompute, color_dark_ground, color_light_ground
    move_camera(player.x, player.y)
    if fov_recompute:
        fov_recompute = False
        libtcod.map_compute_fov(fov_map, player.x, player.y, TORCH_RADIUS, FOV_LIGHT_WALLS, FOV_ALGO)
        libtcod.console_clear(con)
        for y in range(CAMERA_HEIGHT):
            for x in range(CAMERA_WIDTH):
                (map_x, map_y) = (camera_x + x, camera_y + y)
                visible = libtcod.map_is_in_fov(fov_map, map_x, map_y)
                wall = map[map_x][map_y].block_sight
                if not visible:
                    if map[map_x][map_y].explored:
                        if wall:
                            libtcod.console_set_char_background(con, x, y, color_dark_wall, libtcod.BKGND_SET)
                        else:
                            libtcod.console_set_char_background(con, x, y, color_dark_ground, libtcod.BKGND_SET)
                else:
                    if wall:
                        libtcod.console_set_char_background(con, x, y, color_light_wall, libtcod.BKGND_SET)
                    else:
                        libtcod.console_set_char_background(con, x, y, color_light_ground, libtcod.BKGND_SET)
                    map[map_x][map_y].explored = True

    for object in objects:
        if object != player:
            object.draw()
    player.draw()
    libtcod.console_set_default_foreground(con, libtcod.white)
    libtcod.console_print_ex(con, 1, 0, libtcod.BKGND_NONE, libtcod.LEFT, "                   ")
    libtcod.console_print_ex(con, 1, 0, libtcod.BKGND_NONE, libtcod.LEFT, get_player_pos())
    # Bliting console
    libtcod.console_blit(con, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0, 0)

    # Panel Stuff
    libtcod.console_set_default_background(panel, libtcod.black)
    libtcod.console_clear(panel)
    y = 1
    for (line, color) in game_msgs:
        libtcod.console_set_default_foreground(panel, color)
        libtcod.console_print_ex(panel, MSG_X, y, libtcod.BKGND_NONE, libtcod.LEFT, line)
        y += 1
    libtcod.console_print_ex(panel, 1, 3, libtcod.BKGND_NONE, libtcod.LEFT, 'Dungeon level ' + str(dungeon_level))
    render_bar(1, 1, BAR_WIDTH, 'Health', player.fighter.hp, player.fighter.max_hp, color_health_bar_green, libtcod.darker_red)
    libtcod.console_set_default_foreground(panel, libtcod.white)
    libtcod.console_print_ex(panel, 1, 0, libtcod.BKGND_NONE, libtcod.LEFT, get_names_under_mouse())
    # Blit Panel
    libtcod.console_blit(panel, 0, 0, SCREEN_WIDTH, PANEL_HEIGHT, 0, 0, PANEL_Y)


def message(new_msg, color=libtcod.white):
    new_msg_lines = textwrap.wrap(new_msg, MSG_WIDTH)
    for line in new_msg_lines:
        if len(game_msgs) == MSG_HEIGHT:
            del game_msgs[0]
        game_msgs.append((line, color))


def p_m_a(dx, dy):
    global fov_recompute
    x = player.x + dx
    y = player.y + dy

    target = None
    for object in objects:
        if object.fighter and object.x == x and object.y == y:
            target = object
            break
    if target is not None:
        player.fighter.attack(target)
    else:
        player.move(dx, dy)
        fov_recompute = True


def menu(header, options, width):
    if len(options) > INVENTORY_SIZE:
        raise ValueError('Cannot have more than ' + str(INVENTORY_SIZE))
    header_height = libtcod.console_get_height_rect(con, 0, 0, width, SCREEN_HEIGHT, header)
    if header == '':
        header_height = 0
    height = len(options) + header_height
    window = libtcod.console_new(width, height)
    libtcod.console_set_default_foreground(window, libtcod.white)
    libtcod.console_print_rect_ex(window, 0, 0, width, height, libtcod.BKGND_NONE, libtcod.LEFT, header)
    y = header_height
    letter_index = ord('a')
    for option_text in options:
        text = '(' + chr(letter_index) + ') ' + option_text
        libtcod.console_print_ex(window, 0, y, libtcod.BKGND_NONE, libtcod.LEFT, text)
        y += 1
        letter_index += 1
    x = SCREEN_WIDTH / 2 - width / 2
    y = SCREEN_HEIGHT / 2 - height / 2
    libtcod.console_blit(window, 0, 0, width, height, 0, x, y, 1.0, 0.7)
    libtcod.console_flush()
    key = libtcod.console_wait_for_keypress(True)
    if key.vk == libtcod.KEY_ENTER and key.lalt:  # (special case) Alt+Enter: toggle fullscreen
        libtcod.console_set_fullscreen(not libtcod.console_is_fullscreen())
    index = key.c - ord('a')
    if index >= 0 and index < len(options):
        return index
    return None


def inventory_menu(header):
    if len(inventory) == 0:
        options = ['Inventory is empty.']
    else:
        options = []
        for item in inventory:
            text = item.name
            if item.equipment and item.equipment.is_equipped:
                text = text + ' (on ' + item.equipment.is_equipped + ')'
            options.append(text)
    index = menu(header, options, INVENTORY_WIDTH)
    if index is None or len(inventory) == 0:
        return None
    return inventory[index].item


def msgbox(text, width=50):
    menu(text, [], width)


#############################################
# Key/Mouse Handling
#############################################


def handle_keys():
    global key

    key_car = chr(key.c)

    if key_car == ' ':
        for object in objects:
            if object.x == player.x and object.y == player.y and object.item:
                object.item.pick_up()
                break
            elif stairs.x == player.x and stairs.y == player.y:
                next_level()
    if key.vk == libtcod.KEY_ENTER and key.lalt:
        # Alt+Enter Fullscreen
        libtcod.console_set_fullscreen(not libtcod.console_is_fullscreen())
    elif key.vk == libtcod.KEY_ESCAPE:
        return 'exit'  # exit game
    # movement Keys
    if game_state == 'play':
        if player.wait > 0:
            player.wait -= 1
            return
        if libtcod.console_is_key_pressed(libtcod.KEY_UP) and libtcod.console_is_key_pressed(libtcod.KEY_LEFT):
            p_m_a(-1, -1)
        elif libtcod.console_is_key_pressed(libtcod.KEY_UP) and libtcod.console_is_key_pressed(libtcod.KEY_RIGHT):
            p_m_a(1, -1)
        elif libtcod.console_is_key_pressed(libtcod.KEY_DOWN) and libtcod.console_is_key_pressed(libtcod.KEY_LEFT):
            p_m_a(-1, 1)
        elif libtcod.console_is_key_pressed(libtcod.KEY_DOWN) and libtcod.console_is_key_pressed(libtcod.KEY_RIGHT):
            p_m_a(1, 1)
        elif libtcod.console_is_key_pressed(libtcod.KEY_UP):
            p_m_a(0, -1)
        elif libtcod.console_is_key_pressed(libtcod.KEY_DOWN):
            p_m_a(0, 1)
        elif libtcod.console_is_key_pressed(libtcod.KEY_LEFT):
            p_m_a(-1, 0)
        elif libtcod.console_is_key_pressed(libtcod.KEY_RIGHT):
            p_m_a(1, 0)
        else:
            key_char = chr(key.c)

            if key_char == 'i':
                chosen_item = inventory_menu('Press corrisponding key to use, or other to cancel.\n')
                if chosen_item is not None:
                    chosen_item.use()
            if key_char == 'd':
                chosen_item = inventory_menu('Press corrisponding key to drop, or other to cancel.\n')
                if chosen_item is not None:
                    chosen_item.drop()
            if key_char == 'c':
                level_up_xp = LEVEL_UP_BASE + player.level * LEVEL_UP_FACTOR
                msgbox('Character Information\n\nLevel: ' + str(player.level) + '\nXP: ' + str(player.fighter.xp) + '\nXP target: ' + str(level_up_xp) + '\n\nMax HP: ' + str(player.fighter.max_hp) + '\nSTR: ' + str(player.fighter.power) + '\nDEF: ' + str(player.fighter.defense), CHARACTER_SCREEN_WIDTH)
            return 'no turn'


def next_level():
    global dungeon_level
    message('You dive deeper into bithell!', libtcod.green)
    dungeon_level += 1
    make_map()
    initialize_fov()


def random_choice_index(chances):
    dice = libtcod.random_get_int(0, 1, sum(chances))
    running_sum = 0
    choice = 0
    for w in chances:
        running_sum += w
        if dice <= running_sum:
            return choice
        choice += 1


def random_choice(chances_dict):
    chances = chances_dict.values()
    strings = chances_dict.keys()
    return strings[random_choice_index(chances)]


def from_dungeon_level(table):

    for (value, level) in reversed(table):
        if dungeon_level >= level:
            return value
    return 0


def player_death(player):
    global game_state
    message('Player Died', libtcod.orange)
    player.fighter.hp = player.fighter.max_hp
    # game_state = 'dead'
    # player.char = '%'
    # player.color = libtcod.dark_red


def monster_death(monster):
    message(monster.name.capitalize() + ' is dead!', libtcod.light_blue)
    monster.char = '%'
    monster.color = libtcod.dark_red
    monster.blocks = False
    monster.fighter = None
    monster.ai = None
    monster.name = 'remains of ' + monster.name
    monster.send_to_back()


def target_tile(max_range=None):
    global key, mouse
    while True:
        libtcod.console_flush()
        libtcod.sys_check_for_event(libtcod.EVENT_KEY_PRESS | libtcod.EVENT_MOUSE, key, mouse)
        render_all()
        (x, y) = (mouse.cx, mouse.cy)
        (x, y) = (camera_x + x, camera_y + y)
        if mouse.rbutton_pressed or key.vk == libtcod.KEY_ESCAPE:
            return (None, None)
        if (mouse.lbutton_pressed and libtcod.map_is_in_fov(fov_map, x, y) and (max_range is None or player.distance(x, y) <= max_range)):
            return (x, y)


def target_monster(max_range=None):
    while True:
        (x, y) = target_tile(max_range)
        if x is None:
            return None
        for obj in objects:
            if obj.x == x and obj.y == y and obj.fighter and obj != player:
                return obj


def check_level_up():
    level_up_xp = LEVEL_UP_BASE + player.level * LEVEL_UP_FACTOR
    if player.fighter.xp >= level_up_xp:
        player.level += 1
        player.fighter.xp -= level_up_xp
        message('POWER UP! Level: ' + str(player.level) + '!', libtcod.yellow)
        choice = None
        while choice is None:
            choice = menu('Level up! Choose a stat to raise:\n', ['HEALTH: ' + str(player.fighter.max_hp) + '>' + str(player.fighter.max_hp + 25), 'STR: ' + str(player.fighter.power) + '>' + str(player.fighter.power + 2), 'DEF: ' + str(player.fighter.defense) + '>' + str(player.fighter.defense + 3)], LEVEL_SCREEN_WIDTH)
        if choice == 0:
            player.fighter.base_max_hp += 25
        elif choice == 1:
            player.fighter.base_power += 2
        elif choice == 2:
            player.fighter.base_defense += 3


def cast_rage():
    message('Steriods in a bottle...yum!', libtcod.light_violet)
    player.fighter.rage(pow=POWER_BOTTLE)


def cast_srage():
    message('Steriods in a bottle...yum!', libtcod.light_violet)
    player.fighter.rage(pow=POWER_BOTTLE * 2, denf=POWER_BOTTLE)


def cast_heal():
    if player.fighter.hp == player.fighter.max_hp:
        message('Fully Healed Already!', libtcod.red)
        return 'cancelled'
    message('Taste kinda bitter...', libtcod.light_violet)
    player.fighter.heal(HEAL_AMOUNT)


def cast_fireball():
    message('Left-Click to target fireball, Right-Click to cancel.', libtcod.light_cyan)
    (x, y) = target_tile()
    if x is None:
        return 'cancelled'
    for obj in objects:
        if obj.distance(x, y) <= FIREBALL_RADIUS and obj.fighter:
            message(obj.name + ' gets Fireballed for ' + str(FIREBALL_DAMAGE) + ' damage!', libtcod.orange)
            obj.fighter.take_damage(FIREBALL_DAMAGE)


def cast_lightning():
    monster = closet_monster(LIGHTNING_RANGE)
    if monster is None:
        message('No enemy close enough', libtcod.red)
        return 'cancelled'
    message('Smited ' + monster.name + ' for ' + str(LIGHTNING_DAMAGE) + ' damage', libtcod.light_blue)
    monster.fighter.take_damage(LIGHTNING_DAMAGE)


def closet_monster(max_range):
    closest_enemy = None
    closest_dist = max_range + 1
    for object in objects:
        if object.fighter and not object == player and libtcod.map_is_in_fov(fov_map, object.x, object.y):
            dist = player.distance_to(object)
            if dist < closest_dist:
                closest_enemy = object
                closest_dist = dist
    return closest_enemy


def cast_confuse():
    message('Left-Click to target fireball, Right-Click to cancel.', libtcod.light_cyan)
    monster = target_monster(CONFUSE_RANGE)
    if monster is None:
        return 'cancelled'
    old_ai = monster.ai
    monster.ai = ConfusedMonster(old_ai)
    monster.ai.owner = monster
    message('Confused ' + monster.name + ' !', libtcod.light_green)


def save_game():
    file_ = shelve.open('savegame.sage', 'n')
    file_['map'] = map
    file_['objects'] = objects
    file_['inventory'] = inventory
    file_['player_index'] = objects.index(player)
    file_['game_msgs'] = game_msgs
    file_['game_state'] = game_state
    file_['stairs_index'] = objects.index(stairs)
    file_['dungeon_level'] = dungeon_level
    file_.close()


def load_game():
    global map, objects, player, inventory, game_msgs, game_state, stairs, dungeon_level
    file_ = shelve.open('savegame.sage', 'r')
    map = file_['map']
    objects = file_['objects']
    player = objects[file_['player_index']]
    inventory = file_['inventory']
    game_msgs = file_['game_msgs']
    game_state = file_['game_state']
    stairs = objects[file_['stairs_index']]
    dungeon_level = file_['dungeon_level']
    file_.close()
    initialize_fov()


def new_game():
    global player, inventory, game_msgs, game_state, dungeon_level
    f_component = Fighter(hp=100, defense=1, power=4, xp=0, death_function=player_death)
    player = Object(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, '@', 'player', libtcod.white, blocks=True, fighter=f_component, speed=PLAYER_SPEED)  # make The palyer
    player.level = 1
    dungeon_level = 1
    make_map()
    initialize_fov()
    game_state = 'play'  # Playing Game
    inventory = []      # Inventory
    game_msgs = []
    message(GAME_MESSAGE, libtcod.green)  # Game Hi


def initialize_fov():
    global fov_recompute, fov_map
    fov_recompute = True
    fov_map = libtcod.map_new(MAP_WIDTH, MAP_HEIGHT)
    for y in range(MAP_HEIGHT):
        for x in range(MAP_WIDTH):
            libtcod.map_set_properties(fov_map, x, y, not map[x][y].blocked, not map[x][y].block_sight)
    libtcod.console_clear(con)


def play_game():
    global camera_x, camera_y, key, mouse

    player_action = None

    mouse = libtcod.Mouse()
    key = libtcod.Key()

    (camera_x, camera_y) = (0, 0)

    while not libtcod.console_is_window_closed():
        libtcod.sys_check_for_event(libtcod.EVENT_KEY_PRESS | libtcod.EVENT_MOUSE, key, mouse)
        # Render
        render_all()
        libtcod.console_flush()
        check_level_up()
        # Erase Old Objects
        for object in objects:
            object.clear()

        # Check keys
        player_action = handle_keys()
        # Save Game
        if player_action == 'exit':
            save_game()
            break
        # Monster Movement
        if game_state == 'play':
            for object in objects:
                if object.ai:
                    if object.wait > 0:
                        object.wait -= 1
                    else:
                        object.ai.take_turn()


def main_menu():
    img = libtcod.image_load('background.png')

    while not libtcod.console_is_window_closed():
        libtcod.image_blit_2x(img, 0, 0, 0)
        libtcod.console_set_default_foreground(0, libtcod.cyan)
        libtcod.console_print_ex(0, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 - 4, libtcod.BKGND_NONE, libtcod.CENTER, 'TROUBLE IN BIT DUNGEON!')
        libtcod.console_print_ex(0, SCREEN_WIDTH / 2, SCREEN_HEIGHT - 2, libtcod.BKGND_NONE, libtcod.CENTER, '~~SAGE MARTIN~~')
        choice = menu('', ['Play New Game', 'Continue Last Game', 'Quit'], 24)
        if choice == 0:
            new_game()
            play_game()
        if choice == 1:
            try:
                load_game()
            except:
                msgbox('\n No saved game to load! \n', 24)
                continue
            play_game()
        elif choice == 2:
            break

libtcod.console_set_custom_font('arial10x10.png', libtcod.FONT_TYPE_GREYSCALE | libtcod.FONT_LAYOUT_TCOD)
libtcod.console_init_root(SCREEN_WIDTH, SCREEN_HEIGHT, 'Python', False)
libtcod.console_set_fullscreen(not libtcod.console_is_fullscreen())
libtcod.sys_set_fps(LIMIT_FPS)
con = libtcod.console_new(MAP_WIDTH, MAP_HEIGHT)
panel = libtcod.console_new(SCREEN_WIDTH, PANEL_HEIGHT)
#############################################
# Main Loop
#############################################
main_menu()
